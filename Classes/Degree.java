import java.util.ArrayList;
import java.util.List;

public class Degree {
	
	private StudentRecord record;	

	public Degree(StudentRecord r) 
	{
	    // check list size for accumulated grades
		if(grades.size() < 8 || grades.size() > 8)
		        throw new IllegalArgumentException("Amount of grades does not match required amount needed for grading");
    	
    	else record = r;
	}
	
	
	// Classify Degree classification accordingly based on whether the profile is clear or not
	private Classification classifyGrade(Classification first_grade, Classification second_grade) {
	    	
	  Classification classification = null;
	   
	  switch(first_grade) 
	  {
		case First:
		if (second_grade == Classification.UpperSecond) 
			classification = first_grade;
		
		else if(second_grade == Classification.LowerSecond)
		    classification == Classification.UpperSecond;
			break;
		
		case UpperSecond: 
			if (second_grade == Classification.LowerSecond)
			    classification = first_grade;
			
			else if(second_grade == Classification.Third)
			    classification == Classification.LowerSecond;
			break;
			
		case LowerSecond:
			if (second_grade == Classification.Third)
			    classification = first_grade;
			break;
			
		default:
			classification = Classification.Third;
		}

		return classification;
	}
	
	public Classification classify() 
	{
		//initialise list to store grades and boolean to check profile clarity
		List<PointGrade> grades = new ArrayList<PointGrade>();
		
		//create new profiles, level5 and level6
		Profile level6 = new Profile(record.getYear(3));
		
		//get grades level5(year 2 and 3) & level6(year 3)
		grades.addAll(record.getYear(2));
		grades.addAll(record.getYear(3));
		Profile level5 = new Profile(grades);
		
		//create and initialise variable classification to store grades 
		Classification classification = null;
		
		//check grades for equality, if both profiles have the same classification, return that classification
	
		if(level6.classify().equals(level5.classify()))
		{
		 classification = level6.classify();
		}	
		// if level6 profile is clear and level5 is a class below then return level6 qualification
		else if (level6.isClear()) 
		{ 
			classification = classifyGrade(level6.classify(), level5.classify());
		}
		//if level5 profile is clear and level6 profile is a class below return the level5 qualification
		else if (level5.isClear()) 
		{
			classification = classifyGrade(level5.classify(), level6.classify());
		} 
		
		return classification;  						//return profiles classification with classification stored 
	}
}


