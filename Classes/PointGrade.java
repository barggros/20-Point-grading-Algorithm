public class PointGrade 
{	
	private int points;
	
	public String toString() 
	{
		return Integer.toString(points);
	}
	
	public int getPoints() 
	{
		return points;
	}

    // PointGrade constructor
	public PointGrade(int point) throws IllegalArgumentException 
	{
		if ((point < 1) || (point > 19)) 
				throw new IllegalArgumentException();
		
		else points = p;
	}

	//Classify grades converted in fromNumGrade to its right qualification
	public Classification classify() {
		if (points > 0 && points < 5) return Classification.First; 
		else if (points > 4 && points < 9) return Classification.UpperSecond;
		else if (points > 8 && points < 13) return Classification.LowerSecond;
		else if (points > 12 && points < 17) return Classification.Third;
		else return	Classification.Fail;
	}
	
	//convert percentage of grades from 1-100 to the correct grade in 20-point grade scale 
	//and return PointGrade instance with its grade."20 -> is ignored as this scale is from 1-19 
	public static PointGrade fromNumGrade(int g) throws IllegalArgumentException 
	{
		if (g < 1 || g > 100)
			throw new IllegalArgumentException();

		else 
		{
				if ((g > 0 && g < 30)) g = 19;
				else if ((g > 29 && g < 35)) g = 18;
				else if ((g > 34 && g < 40)) g = 17;
				else if ((g > 39 && g < 42)) g = 16;
				else if ((g > 41 && g < 45)) g = 15;
				else if ((g > 44 && g < 47)) g = 14;
				else if ((g > 47 && g < 50)) g = 13;
				else if ((g > 49 && g < 52)) g = 12;
				else if ((g > 51 && g < 55)) g = 11;
				else if ((g > 54 && g < 57)) g = 10;
				else if ((g > 56 && g < 60)) g = 9;
				else if ((g > 59 && g < 62)) g = 8;
				else if ((g > 61 && g < 65)) g = 7;
				else if ((g > 64 && g < 67)) g = 6;
				else if ((g > 66 && g < 70)) g = 5;
				else if ((g > 69 && g < 73)) g = 4;
				else if ((g > 73 && g < 76)) g = 3;
				else if ((g > 75 && g < 79)) g = 2;
				else if ((g > 78 && g < 101)) g = 1;		
		}
		
		return new PointGrade(g);									//return new instance of PointGrade with stored grades
	}
}





