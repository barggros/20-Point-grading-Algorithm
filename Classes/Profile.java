import java.util.Arrays; 
import java.util.List;
import java.util.HashMap;

public class Profile {

	private List<PointGrade> grades;
	
	public String toString() 
	{
		return Arrays.toString(grades.toArray());
	}

	// constructor receives a list of grades to insert in grade
	public Profile(List<PointGrade> g) 
	{
		//extract grades and set boundary of list of grades to no less than 4 and no more than 8
	    for (int i = 0; i < g.size(); i++)
	    {
		    if ((g.size() <= 3 || g.size() >= 9)) 
		        throw new IllegalArgumentException();
		    
		    else if (g.get(i).equals(Classification.Fail)) 
		        throw new IllegalArgumentException("Grades with Fail cannot be classified");
	    }
	    
	    grades = g;
	}
	
		//set grades classification in HashMap appropriately
	public void setMap (HashMap<Classification, Integer> freq, List<PointGrade> studentgrades)
	{
	  for (int i = 0; i < studentgrades.size(); i++)
	  {
			
			if (freq.containsKey(studentgrades.get(i).classify()))
			    freq.put(studentgrades.get(i).classify(), freq.get(studentgrades.get(i).classify()) +1);
			
			
			else freq.put(studentgrades.get(i).classify(), 1);
		}
		
		// check classification (keys) in map for null value, if null swap to 0	
    	if (freq.get(Classification.First) == null) 
    	        freq.put(Classification.First, 0); 
    	        
    	else if (freq.get(Classification.UpperSecond) == null) 
    	        freq.put(Classification.UpperSecond, 0); 
    	        
    	else if (freq.get(Classification.LowerSecond) == null)
    	        freq.put(Classification.LowerSecond, 0); 
    	        
    	else if (freq.get(Classification.Third) == null) 
    	        freq.put(Classification.Third, 0);
	}
				
	//check profile by frequency, if frequency of Third is <= of 1/4 of grades
	//if is then profile is clear, else false Borderline
	public boolean isClear() 
	{
		HashMap<Classification, Integer> freq = new HashMap<Classification, Integer>();
		
		//add classification and frequency to the HashMap
		setMap (freq, grades);
		
		// get required frequencies for clear checks
		int first = freq.get(Classification.First);
		int upper = freq.get(Classification.First) + freq.get(Classification.UpperSecond);
		
		// variable names do as stated
		int onefourth = grades.size() / 4;
		int half = grades.size() / 2;
				
		//check both First and UpperSecond for clarity, if clear return true else false, other profiles are always true
		if ((first > half) || (first == half && (freq.get(Classification.Third) <= onefourth)))
		    return true; 
		
		else if (first == half && (freq.get(Classification.Third) == half)) 
		{
			System.out.println("Borderline");
			return false;
		} 
		
		else if ((upper > half) || (upper == half && (freq.get(Classification.Third) <= onefourth)))
			return true;
		
		else if (upper == half && (freq.get(Classification.Third) == half)) 
		{
			System.out.println("Borderline");
			return false;
		}
		
		else return true;
	}
	
	public Classification classify() 
	{
		
		//initialise HashMap
		HashMap<Classification, Integer> frequencies = new HashMap<Classification, Integer>();		
		
		//add frequencies with respective classification key
		setMap (frequencies, grades);
		
		//calculate the result of half the list and initialise variables
		int result = grades.size() / 2;
		int first=0, usecond=0, lsecond=0;
		
		// get required frequencies for clear checks
		first = frequencies.get(Classification.First);
		usecond = frequencies.get(Classification.UpperSecond);
		lsecond = frequencies.get(Classification.LowerSecond);
		
		//addition of frequencies for grades Upper and Lower
		int upperSecond = first + usecond;
		int lowerSecond = first + usecond + lsecond;
		
		// if classification is 50% return that classification according to specifications
		if (first >= result) 
		    return Classification.First;
		    
		else if (upperSecond >= result) 
		    return Classification.UpperSecond;
		    
		else if (lowerSecond >= result)
		    return Classification.LowerSecond;
		    
		else return Classification.Third;
	}
}






