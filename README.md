# 20-Point Grading Algorithm

-------

This Algorithm is based on the popular 20-point grading scale of the UK, where the grading is simple to grasp, as such is the following:


* Grades from 1 - 4 First Class 
* Grades from 5 - 8 Upper Second
* Grades from 9 - 12 Lower Second
* Grades from 13 - 16 Third 
* Grades from 17-20 Fail


__The percentages ar as follow:__


* First class   70%-100%
* Upper Second  60%-69%
* Lower Second  50%-59%
* Third         40%-49%
* Fail 	      1%-39%


Implemented with Java, Originally in Eclipse.

__Test Classes (JUnit Framework)__

+ Unit tests are provided to test each method.
+ Unit tests test the boundary of each grade or grade % result.
 

(A more Accurate Scale will be added soon) 

