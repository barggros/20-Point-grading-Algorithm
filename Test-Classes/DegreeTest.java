package classes;

import static org.junit.Assert.*; 
import org.junit.Test;



public class DegreeTest {
	
	//Testing level5 for first class of degree classification
	@Test
	public void test1() {
		
		StudentRecord r = new StudentRecord();
		r.addGradeToYear(new PointGrade(1), 2);
		r.addGradeToYear(new PointGrade(1), 2);
		r.addGradeToYear(new PointGrade(2), 2);
		r.addGradeToYear(new PointGrade(2), 2);
		
		r.addGradeToYear(new PointGrade(8), 3);
		r.addGradeToYear(new PointGrade(8), 3);
		r.addGradeToYear(new PointGrade(16), 3);
		r.addGradeToYear(new PointGrade(13), 3);
		
		assertEquals(new Degree(r).classify(), Classification.First);
	}
	
	//Testing boundary of UpperSecond class of degree classification
	@Test
	public void test2() {

		StudentRecord r = new StudentRecord();
		r.addGradeToYear(new PointGrade(5), 2);
		r.addGradeToYear(new PointGrade(5), 2);
		r.addGradeToYear(new PointGrade(6), 2);
		r.addGradeToYear(new PointGrade(6), 2);
		
		r.addGradeToYear(new PointGrade(7), 3);
		r.addGradeToYear(new PointGrade(7), 3);
		r.addGradeToYear(new PointGrade(8), 3);
		r.addGradeToYear(new PointGrade(8), 3);
		
		assertEquals(new Degree(r).classify(), Classification.UpperSecond);
	}
	
	//Testing boundary of LowerSecond class of degree classification
	@Test
	public void test3() {

		StudentRecord r = new StudentRecord();
		r.addGradeToYear(new PointGrade(10), 2);
		r.addGradeToYear(new PointGrade(10), 2);
		r.addGradeToYear(new PointGrade(9), 2);
		r.addGradeToYear(new PointGrade(9), 2);
		
		r.addGradeToYear(new PointGrade(11), 3);
		r.addGradeToYear(new PointGrade(11), 3);
		r.addGradeToYear(new PointGrade(12), 3);
		r.addGradeToYear(new PointGrade(12), 3);
		
		assertEquals(new Degree(r).classify(), Classification.LowerSecond);
	}
	
	//Testing boundary of Third class of degree classification
	@Test
	public void test4() {
		
		StudentRecord r = new StudentRecord();
		r.addGradeToYear(new PointGrade(13), 2);
		r.addGradeToYear(new PointGrade(13), 2);
		r.addGradeToYear(new PointGrade(14), 2);
		r.addGradeToYear(new PointGrade(14), 2);
		
		r.addGradeToYear(new PointGrade(15), 3);
		r.addGradeToYear(new PointGrade(15), 3);
		r.addGradeToYear(new PointGrade(16), 3);
		r.addGradeToYear(new PointGrade(16), 3);
		
		assertEquals(new Degree(r).classify(), Classification.Third);
	}
	
	//Testing boundary of Third class of degree classification
	@Test
	public void testthird2() {

		StudentRecord r = new StudentRecord();
		r.addGradeToYear(new PointGrade(16), 2);
		r.addGradeToYear(new PointGrade(16), 2);
		r.addGradeToYear(new PointGrade(15), 2);
		r.addGradeToYear(new PointGrade(15), 2);
		
		r.addGradeToYear(new PointGrade(11), 3);
		r.addGradeToYear(new PointGrade(13), 3);
		r.addGradeToYear(new PointGrade(14), 3);
		r.addGradeToYear(new PointGrade(14), 3);
		
		assertEquals(new Degree(r).classify(), Classification.Third);
	}
	
	//Testing degree for level5 class classification LowerSecond but returns LowerSecond of level6 
	//when level5 classification is better (implementation bug)
	@Test
	public void test5() {
		
		StudentRecord r = new StudentRecord();
		r.addGradeToYear(new PointGrade(9), 2);
		r.addGradeToYear(new PointGrade(9), 2);
		r.addGradeToYear(new PointGrade(12), 2);
		r.addGradeToYear(new PointGrade(12), 2);
		
		r.addGradeToYear(new PointGrade(12), 3);
		r.addGradeToYear(new PointGrade(12), 3);
		r.addGradeToYear(new PointGrade(16), 3);
		r.addGradeToYear(new PointGrade(16), 3);
		
		assertEquals(new Degree(r).classify(), Classification.LowerSecond);
	}
	
	//Testing for level5 LowerSecond class classification 
	@Test
	public void test6() {
		
		StudentRecord r = new StudentRecord();
		r.addGradeToYear(new PointGrade(16), 2);
		r.addGradeToYear(new PointGrade(16), 2);
		r.addGradeToYear(new PointGrade(13), 2);
		r.addGradeToYear(new PointGrade(13), 2);
		
		r.addGradeToYear(new PointGrade(9), 3);
		r.addGradeToYear(new PointGrade(9), 3);
		r.addGradeToYear(new PointGrade(12), 3);
		r.addGradeToYear(new PointGrade(12), 3);
		
		assertEquals(new Degree(r).classify(), Classification.LowerSecond);
	}
	
	//Testing for level6 LowerSecond class classification for First class
	@Test
	public void testThird() {

		StudentRecord r = new StudentRecord();
		r.addGradeToYear(new PointGrade(12), 2);
		r.addGradeToYear(new PointGrade(12), 2);
		r.addGradeToYear(new PointGrade(13), 2);
		r.addGradeToYear(new PointGrade(13), 2);
		
		r.addGradeToYear(new PointGrade(1), 3);
		r.addGradeToYear(new PointGrade(1), 3);
		r.addGradeToYear(new PointGrade(8), 3);
		r.addGradeToYear(new PointGrade(8), 3);
		
		assertEquals(new Degree(r).classify(), Classification.First);
	}
	
	//Testing level6 for degree class classification LowerSecond
	@Test
	public void test7() {
		
		StudentRecord r = new StudentRecord();
		r.addGradeToYear(new PointGrade(9), 2);
		r.addGradeToYear(new PointGrade(9), 2);
		r.addGradeToYear(new PointGrade(13), 2);
		r.addGradeToYear(new PointGrade(13), 2);
		
		r.addGradeToYear(new PointGrade(9), 3);
		r.addGradeToYear(new PointGrade(9), 3);
		r.addGradeToYear(new PointGrade(12), 3);
		r.addGradeToYear(new PointGrade(12), 3);
		
		assertEquals(new Degree(r).classify(), Classification.LowerSecond);
	}
	
	//Test level6 profile for a UpperSecond class degree classification
	@Test
	public void test8() {
		
		StudentRecord r = new StudentRecord();
		r.addGradeToYear(new PointGrade(13), 2);
		r.addGradeToYear(new PointGrade(9), 2);
		r.addGradeToYear(new PointGrade(13), 2);
		r.addGradeToYear(new PointGrade(13), 2);
		
		r.addGradeToYear(new PointGrade(5), 3);
		r.addGradeToYear(new PointGrade(5), 3);
		r.addGradeToYear(new PointGrade(9), 3);
		r.addGradeToYear(new PointGrade(9), 3);
		
		assertEquals(new Degree(r).classify(), Classification.UpperSecond);
	}

}
