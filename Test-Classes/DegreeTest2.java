package classes;

import static org.junit.Assert.*;

import org.junit.Test;

public class DegreeTest2 {

	//Testing level5 for a UpperSecond but test fails - level5 as clear and lvl6 as failed
		@Test
		public void test1() {

			StudentRecord r = new StudentRecord();
			r.addGradeToYear(new PointGrade(5), 2);
			r.addGradeToYear(new PointGrade(5), 2);
			r.addGradeToYear(new PointGrade(5), 2);
			r.addGradeToYear(new PointGrade(5), 2);
			
			r.addGradeToYear(new PointGrade(12), 3);
			r.addGradeToYear(new PointGrade(12), 3);
			r.addGradeToYear(new PointGrade(12), 3);
			r.addGradeToYear(new PointGrade(12), 3);
	
			assertEquals(new Degree(r).classify(), Classification.UpperSecond);
			assertEquals(new Profile(r.getYear(3)).classify(), Classification.LowerSecond);
			//assertEquals(new Degree(r).classify(), Classification.UpperSecond);
	
		}
		
		//Testing for level5 LowerSecond class degree classification but test fails
		@Test
		public void test2() {

			StudentRecord r = new StudentRecord();
			r.addGradeToYear(new PointGrade(10), 2);
			r.addGradeToYear(new PointGrade(10), 2);
			r.addGradeToYear(new PointGrade(11), 2);
			r.addGradeToYear(new PointGrade(11), 2);
			
			r.addGradeToYear(new PointGrade(10), 3);
			r.addGradeToYear(new PointGrade(13), 3);
			r.addGradeToYear(new PointGrade(13), 3);
			r.addGradeToYear(new PointGrade(13), 3);
			
			assertEquals(new Degree(r).classify(), Classification.LowerSecond);
		}
		
		//Test for level6 qualification for a LowerSecond class degree qualification
			@Test
			public void test9() {

				StudentRecord r = new StudentRecord();
				r.addGradeToYear(new PointGrade(14), 2);
				r.addGradeToYear(new PointGrade(14), 2);
				r.addGradeToYear(new PointGrade(13), 2);
				r.addGradeToYear(new PointGrade(13), 2);
				
				r.addGradeToYear(new PointGrade(9), 3);
				r.addGradeToYear(new PointGrade(9), 3);
				r.addGradeToYear(new PointGrade(10), 3);
				r.addGradeToYear(new PointGrade(13), 3);
				
				assertEquals(new Degree(r).classify(), Classification.LowerSecond);
			}
			
			//Testing for third class on both level5 and level6 qualifications expected result Third class
			@Test
			public void test10() {
				
				StudentRecord r = new StudentRecord();
				r.addGradeToYear(new PointGrade(13), 2);
				r.addGradeToYear(new PointGrade(13), 2);
				r.addGradeToYear(new PointGrade(13), 2);
				r.addGradeToYear(new PointGrade(13), 2);
				
				r.addGradeToYear(new PointGrade(13), 3);
				r.addGradeToYear(new PointGrade(13), 3);
				r.addGradeToYear(new PointGrade(16), 3);
				r.addGradeToYear(new PointGrade(16), 3);
				
				assertEquals(new Degree(r).classify(), Classification.Third);
			}

}
