package classes;

import static org.junit.Assert.*;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Test;



public class PointGradeTest {
	
	//Test for all values of Classification Fail
	@Test
	public void testFail(){
	
		assertEquals(new PointGrade(17).classify(), Classification.Fail);
		assertEquals(new PointGrade(19).classify(), Classification.Fail);
		assertEquals(new PointGrade(18).classify(), Classification.Fail);
		assertEquals(new PointGrade(17).classify(), Classification.Fail);
		
	}
	
	//Testing for all values of Classification Third
	@Test
	public void test1 (){

		assertEquals(new PointGrade(13).classify(), Classification.Third);
		assertEquals(new PointGrade(16).classify(), Classification.Third);
		assertEquals(new PointGrade(14).classify(), Classification.Third);
		assertEquals(new PointGrade(15).classify(), Classification.Third);
		
	}
	
	//Testing for all values of Classification LowerSecond
	@Test
	public void test2() {

		assertEquals(new PointGrade(12).classify(), Classification.LowerSecond);
		assertEquals(new PointGrade(11).classify(), Classification.LowerSecond);
		assertEquals(new PointGrade(10).classify(), Classification.LowerSecond);
		assertEquals(new PointGrade(9).classify(), Classification.LowerSecond);
	}
	
	//Testing for all values of Classification UpperSecond
	@Test
	public void test3() {

		assertEquals(new PointGrade(8).classify(), Classification.UpperSecond);
		assertEquals(new PointGrade(7).classify(), Classification.UpperSecond);
		assertEquals(new PointGrade(6).classify(), Classification.UpperSecond);
		assertEquals(new PointGrade(5).classify(), Classification.UpperSecond);
	}
	
	//Testing for all values of Classification First
	@Test
	public void test4 (){
	
		assertEquals(new PointGrade(4).classify(), Classification.First);	
		assertEquals(new PointGrade(3).classify(), Classification.First);
		assertEquals(new PointGrade(2).classify(), Classification.First);	
		assertEquals(new PointGrade(1).classify(), Classification.First);
	}

}
