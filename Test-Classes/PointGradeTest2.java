package classes;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

	public class PointGradeTest2 {

		@Rule
	    public ExpectedException thrown = ExpectedException.none();
		
		//testing for Exception of grades boundary, grades cannot be less than 1 or more than 19
		@Test(expected= IllegalArgumentException.class)
		public void testException1() {
		
			new PointGrade(20).classify();
			new PointGrade(0).classify();
			
		}
		
		@Test(expected= IllegalArgumentException.class)
		public void testException2() {
			PointGrade.fromNumGrade(0);
			PointGrade.fromNumGrade(101);
			PointGrade.fromNumGrade(102);
			PointGrade.fromNumGrade(103);
		}
		
		// Testing classify for boundary of Fail classification within PointGrade 
		@Test
		public void testFail(){
			assertEquals(PointGrade.fromNumGrade(1).classify(), Classification.Fail);
			assertEquals(PointGrade.fromNumGrade(5).classify(), Classification.Fail);
			assertEquals(PointGrade.fromNumGrade(10).classify(), Classification.Fail);
			assertEquals(PointGrade.fromNumGrade(15).classify(), Classification.Fail);
			assertEquals(PointGrade.fromNumGrade(20).classify(), Classification.Fail);
			assertEquals(PointGrade.fromNumGrade(25).classify(), Classification.Fail);
			assertEquals(PointGrade.fromNumGrade(29).classify(), Classification.Fail);
		}
		
		// Testing classify for boundary of Fail classification within PointGrade
		@Test
		public void testFail2() {
			assertEquals(PointGrade.fromNumGrade(30).classify(), Classification.Fail);
			assertEquals(PointGrade.fromNumGrade(34).classify(), Classification.Fail);
			assertEquals(PointGrade.fromNumGrade(35).classify(), Classification.Fail);
			assertEquals(PointGrade.fromNumGrade(39).classify(), Classification.Fail);
		}
		
		// Testing classify within PointGrade for boundary of Third class classification
		@Test
		public void testThird(){
			assertEquals(PointGrade.fromNumGrade(40).classify(), Classification.Third);
			assertEquals(PointGrade.fromNumGrade(41).classify(), Classification.Third);
			assertEquals(PointGrade.fromNumGrade(42).classify(), Classification.Third);
			assertEquals(PointGrade.fromNumGrade(43).classify(), Classification.Third);
			assertEquals(PointGrade.fromNumGrade(44).classify(), Classification.Third);
			assertEquals(PointGrade.fromNumGrade(45).classify(), Classification.Third);
			assertEquals(PointGrade.fromNumGrade(46).classify(), Classification.Third);
			assertEquals(PointGrade.fromNumGrade(48).classify(), Classification.Third);
			assertEquals(PointGrade.fromNumGrade(49).classify(), Classification.Third);
		}
		
		
		// Testing classify within PointGrade for boundary of LowerSecond class classification
		@Test
		public void testLowerSecond() {
			assertEquals(PointGrade.fromNumGrade(50).classify(), Classification.LowerSecond);
			assertEquals(PointGrade.fromNumGrade(51).classify(), Classification.LowerSecond);
			assertEquals(PointGrade.fromNumGrade(52).classify(), Classification.LowerSecond);
			assertEquals(PointGrade.fromNumGrade(53).classify(), Classification.LowerSecond);
			assertEquals(PointGrade.fromNumGrade(54).classify(), Classification.LowerSecond);
		}
		
		// Testing classify within PointGrade for boundary of LowerSecond class classification
		@Test
		public void testLowerSecond2() {
			assertEquals(PointGrade.fromNumGrade(55).classify(), Classification.LowerSecond);
			assertEquals(PointGrade.fromNumGrade(56).classify(), Classification.LowerSecond);
			assertEquals(PointGrade.fromNumGrade(57).classify(), Classification.LowerSecond);
			assertEquals(PointGrade.fromNumGrade(58).classify(), Classification.LowerSecond);
			assertEquals(PointGrade.fromNumGrade(59).classify(), Classification.LowerSecond);
		}
				
		
		// Testing classify within PointGrade for boundary of UpperSecond class classification
		@Test
		public void testUpperSecond() {
			assertEquals(PointGrade.fromNumGrade(60).classify(), Classification.UpperSecond);
			assertEquals(PointGrade.fromNumGrade(61).classify(), Classification.UpperSecond);
			assertEquals(PointGrade.fromNumGrade(62).classify(), Classification.UpperSecond);
			assertEquals(PointGrade.fromNumGrade(63).classify(), Classification.UpperSecond);
			assertEquals(PointGrade.fromNumGrade(64).classify(), Classification.UpperSecond);
			assertEquals(PointGrade.fromNumGrade(65).classify(), Classification.UpperSecond);
			assertEquals(PointGrade.fromNumGrade(66).classify(), Classification.UpperSecond);
			assertEquals(PointGrade.fromNumGrade(67).classify(), Classification.UpperSecond);
			assertEquals(PointGrade.fromNumGrade(68).classify(), Classification.UpperSecond);
			assertEquals(PointGrade.fromNumGrade(69).classify(), Classification.UpperSecond);
		}
		
		// Testing classify within PointGrade for boundary of first class classification 
		//includes TestFirst1 and TestFirst2
		@Test
		public void testFirst1(){
			assertEquals(PointGrade.fromNumGrade(70).classify(), Classification.First);	
			assertEquals(PointGrade.fromNumGrade(71).classify(), Classification.First);	
			assertEquals(PointGrade.fromNumGrade(72).classify(), Classification.First);	
			assertEquals(PointGrade.fromNumGrade(74).classify(), Classification.First);
			assertEquals(PointGrade.fromNumGrade(75).classify(), Classification.First);
			assertEquals(PointGrade.fromNumGrade(76).classify(), Classification.First);	
			assertEquals(PointGrade.fromNumGrade(77).classify(), Classification.First);	
			assertEquals(PointGrade.fromNumGrade(79).classify(), Classification.First);
		}
		
		//runs in conjunction with above test
		@Test
		public void testFirst2(){
			assertEquals(PointGrade.fromNumGrade(80).classify(), Classification.First);	
			assertEquals(PointGrade.fromNumGrade(83).classify(), Classification.First);
			assertEquals(PointGrade.fromNumGrade(85).classify(), Classification.First);	
			assertEquals(PointGrade.fromNumGrade(99).classify(), Classification.First);	
			assertEquals(PointGrade.fromNumGrade(100).classify(), Classification.First);
		}
		
		//Testing classify for value 77 of First class classification, as it gave an error with the test above
		@Test
		public void testFirst3() {
			assertEquals(PointGrade.fromNumGrade(77).classify(), Classification.First);
		}
	}


