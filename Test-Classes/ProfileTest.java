package classes;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class ProfileTest {
	
	List<PointGrade> g = new ArrayList<PointGrade>(); 
	List<PointGrade> g1 = new ArrayList<PointGrade>();
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	Profile profile1,profile2;
	
	//Test should throw an IllegalArgumentException but it doesn't, either i am testing it wrong or
	//there is a bug in the implementation
	@Test(expected= IllegalArgumentException.class)
	public void testExcept(){
		g.add(new PointGrade(19));
		g.add(new PointGrade(18));
		g.add(new PointGrade(17));
		g.add(new PointGrade(20));
		
		g1.add(new PointGrade(1));
		g1.add(new PointGrade(1));
		g1.add(new PointGrade(17));
		g1.add(new PointGrade(17));

		assertEquals(new Profile(g).classify(), Classification.Fail);
		assertEquals(new Profile(g1).classify(), Classification.Fail);
	}
	
	//Testing for illegalArgumentException for size of Lists containing grades exceeding the boundary limits
	@Test(expected= IllegalArgumentException.class)
	public void testException2(){
		g.add(new PointGrade(0));
		g.add(new PointGrade(18));
		g.add(new PointGrade(17));
		profile1 = new Profile(g);
		profile1.classify();
		
		g1.add(new PointGrade(20));
		g1.add(new PointGrade(19));
		g1.add(new PointGrade(18));
		g1.add(new PointGrade(17));
		g1.add(new PointGrade(17));
		g1.add(new PointGrade(17));
		profile2 = new Profile(g);
		profile2.classify();
	}
	
	
	//Testing for classification First of profiles1 and profile2
	@Test
	public void testing1(){
		g.add(new PointGrade(1));
		g.add(new PointGrade(1));
		g.add(new PointGrade(5));
		g.add(new PointGrade(13));
		
		g1.add(new PointGrade(4));
		g1.add(new PointGrade(4));
		g1.add(new PointGrade(8));
		g1.add(new PointGrade(16));
		
		assertEquals(new Profile(g).classify(), Classification.First);
		assertEquals(new Profile(g1).classify(), Classification.First);
		}
	
	//Testing for classification UpperSecond of profiles1 and profile2
	@Test
	public void testing2(){
			g.add(new PointGrade(5));
			g.add(new PointGrade(5));
			g.add(new PointGrade(9));
			g.add(new PointGrade(13));
			
			g1.add(new PointGrade(8));
			g1.add(new PointGrade(8));
			g1.add(new PointGrade(12));
			g1.add(new PointGrade(16));
			
			assertEquals(new Profile(g).classify(), Classification.UpperSecond);
			assertEquals(new Profile(g1).classify(), Classification.UpperSecond);
		}
	
	//Testing for classification LowerSecond of profiles1 and profile2
	@Test
	public void testing3(){
			g.add(new PointGrade(9));
			g.add(new PointGrade(9));
			g.add(new PointGrade(8));
			g.add(new PointGrade(16));
			
			g1.add(new PointGrade(12));
			g1.add(new PointGrade(12));
			g1.add(new PointGrade(13));
			g1.add(new PointGrade(13));
			
			assertEquals(new Profile(g).classify(), Classification.LowerSecond);
			assertEquals(new Profile(g1).classify(), Classification.LowerSecond);
		}
	
	//Testing for classification Third of profiles1 and profile2
	@Test
	public void testing4(){
			g.add(new PointGrade(13));
			g.add(new PointGrade(1));
			g.add(new PointGrade(16));
			g.add(new PointGrade(16));
			
			g1.add(new PointGrade(5));
			g1.add(new PointGrade(16));
			g1.add(new PointGrade(15));
			g1.add(new PointGrade(15));
			
			assertEquals(new Profile(g).classify(), Classification.Third);
			assertEquals(new Profile(g1).classify(), Classification.Third);
		}
	
	//Testing for classification UpperSecond of profiles1
	@Test
	public void testing5(){
		g1.add(new PointGrade(1));
		g1.add(new PointGrade(5));
		g1.add(new PointGrade(9));
		g1.add(new PointGrade(9));
		
		assertEquals(new Profile(g1).classify(), Classification.UpperSecond);
	}
}
