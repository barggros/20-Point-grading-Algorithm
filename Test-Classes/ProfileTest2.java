package classes;

import static org.junit.Assert.*; 

import java.util.ArrayList;
import java.util.List;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class ProfileTest2 {
	
	//initialising list to hold grades and new instances of profile
	List<PointGrade> g = new ArrayList<PointGrade>(); 
	List<PointGrade> g1 = new ArrayList<PointGrade>();
	Profile profile1,profile2;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
		
	//Testing exception for failed grades, but does not throw exception, there is a bug within the implementation
	//or I made a mistake in the testing, but I don't think is the test 
	@Test(expected= IllegalArgumentException.class)
	public void testException1(){
		g.add(new PointGrade(20));
		g.add(new PointGrade(1));
		g.add(new PointGrade(19));
		g.add(new PointGrade(17));
		profile1 = new Profile(g);
		profile1.classify();
	}
	
	//testing Profile Exception for illegalArgument, but test fails for some reason
	@Test(expected= IllegalArgumentException.class)
	public void testException2(){
		g.add(new PointGrade(0));
		g.add(new PointGrade(17));
		g.add(new PointGrade(18));
		profile1 = new Profile(g);
		profile1.classify();
		
		g1.add(new PointGrade(1));
		g1.add(new PointGrade(1));
		g1.add(new PointGrade(2));
		g1.add(new PointGrade(7));
		g1.add(new PointGrade(8));
		g1.add(new PointGrade(10));
		g1.add(new PointGrade(17));
		g1.add(new PointGrade(10));
		g1.add(new PointGrade(17));
		profile2 = new Profile(g1);
		profile2.classify();
	}
	
	//testing for clear first grades on first profile and unclear on profile2
	@Test
	public void testing1(){
		g.add(new PointGrade(1));
		g.add(new PointGrade(2));
		g.add(new PointGrade(5));
		g.add(new PointGrade(13));
		profile1 = new Profile(g);
		
		g1.add(new PointGrade(3));
		g1.add(new PointGrade(4));
		g1.add(new PointGrade(13));
		g1.add(new PointGrade(16));
		profile2 = new Profile(g1);
		
		assertEquals(profile1.isClear(), true);
		assertEquals(profile2.isClear(), false);
		}
	
	//testing profile for clearness Third and UpperSecond on profile1 and profile2 in order
	@Test
	public void testingfail(){
		g.add(new PointGrade(6));
		g.add(new PointGrade(15));
		g.add(new PointGrade(15));
		g.add(new PointGrade(15));
		profile1 = new Profile(g);
		
		g1.add(new PointGrade(8));
		g1.add(new PointGrade(8));
		g1.add(new PointGrade(9));
		g1.add(new PointGrade(16));
		profile2 = new Profile(g1);
		
		assertEquals(profile1.isClear(), true);
		assertEquals(profile2.isClear(), true);
		}
	
	//Testing for false on both profiles, both are borderline
	@Test
	public void testingfalse(){
		g.add(new PointGrade(1));
		g.add(new PointGrade(1));
		g.add(new PointGrade(16));
		g.add(new PointGrade(13));
		profile1 = new Profile(g);
		
		g1.add(new PointGrade(4));
		g1.add(new PointGrade(4));
		g1.add(new PointGrade(13));
		g1.add(new PointGrade(16));
		profile2 = new Profile(g1);
		
		assertEquals(profile1.isClear(), false);
		assertEquals(profile2.isClear(), false);
		}
	
	//Testing for clear true value on both profiles, testing boundaries of UpperSecond
	@Test
	public void testing2(){
			g.add(new PointGrade(5));
			g.add(new PointGrade(5));
			g.add(new PointGrade(9));
			g.add(new PointGrade(13));
			profile1 = new Profile(g);
			
			g1.add(new PointGrade(8));
			g1.add(new PointGrade(8));
			g1.add(new PointGrade(12));
			g1.add(new PointGrade(16));
			profile2 = new Profile(g1);
			
			assertEquals(profile1.isClear(), true);
			assertEquals(profile2.isClear(), true);
		}
	
	//Testing for LowerSecond Clear(true) and Third Clear(true)
	@Test
	public void testing3(){
			g.add(new PointGrade(9));
			g.add(new PointGrade(9));
			g.add(new PointGrade(13));
			g.add(new PointGrade(16));
			profile1 = new Profile(g);
			
			g1.add(new PointGrade(12));
			g1.add(new PointGrade(13));
			g1.add(new PointGrade(13));
			g1.add(new PointGrade(16));
			profile2 = new Profile(g1);
			
			assertEquals(profile1.isClear(), true);
			assertEquals(profile2.isClear(), true);
		}
	
	//testing boundary of Third for clearness(true) and clearness(true) of LowerSecond in profile1 and profile2 
	@Test
	public void testing4(){
			g.add(new PointGrade(15));
			g.add(new PointGrade(16));
			g.add(new PointGrade(16));
			g.add(new PointGrade(13));
			profile1 = new Profile(g);
			
			g1.add(new PointGrade(9));
			g1.add(new PointGrade(9));
			g1.add(new PointGrade(15));
			g1.add(new PointGrade(15));
			profile2 = new Profile(g1);
			
			assertEquals(profile1.isClear(), true);
			assertEquals(profile2.isClear(), true);
		}
	
	//Testing First grades boundary for clearness(true) and clearness(true) of LowerSecond in profile1 and profile2 
	@Test
	public void testing5(){
			g.add(new PointGrade(1));
			g.add(new PointGrade(4));
			g.add(new PointGrade(5));
			g.add(new PointGrade(13));
			profile1 = new Profile(g);
			
			g1.add(new PointGrade(12));
			g1.add(new PointGrade(12));
			g1.add(new PointGrade(13));
			g1.add(new PointGrade(16));
			profile2 = new Profile(g1);
			
			assertEquals(profile1.isClear(), true);
			assertEquals(profile2.isClear(), true);
		}
	
	//testing boundary of Third for clearness(true) in profile1 and LowerSecond for false value in profile2
	@Test
	public void testing6(){
		
			g.add(new PointGrade(5));
			g.add(new PointGrade(9));
			g.add(new PointGrade(16));
			g.add(new PointGrade(16));
			profile1 = new Profile(g);
			
			g1.add(new PointGrade(1));
			g1.add(new PointGrade(5));
			g1.add(new PointGrade(16));
			g1.add(new PointGrade(16));
			profile2 = new Profile(g1);
			
			assertEquals(profile1.isClear(), true);
			assertEquals(profile2.isClear(), false);
		}
	
	//testing for third within both profiles, expected output true in profile1 and false in profile2 
	@Test
	public void testing7(){
		
			g.add(new PointGrade(13));
			g.add(new PointGrade(13));
			g.add(new PointGrade(13));
			g.add(new PointGrade(13));
			profile1 = new Profile(g);
			
			g1.add(new PointGrade(16));
			g1.add(new PointGrade(16));
			g1.add(new PointGrade(5));
			g1.add(new PointGrade(8));
			profile2 = new Profile(g1);
			
			assertEquals(profile1.isClear(), true);
			assertEquals(profile2.isClear(), false);
		}
	}






